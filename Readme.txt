Introduction: Predicting Airplane Delays

onpremises.ipynb is executed on local desktop on jupyter notebook. The user need python3 to excute them.


The goals of this notebook are:
- Process and create a dataset from downloaded ZIP files
- Exploratory data analysis (EDA)
- Establish a baseline model and improve it

Dataset
The dataset contains date, time, origin, destination, airline, distance, and delay status of flights for flights between 2014 and 2018.
The data are in 60 compressed files, where each file contains a CSV for the flight details in a month for the five years (from 2014 - 2018). 
The data can be downloaded from this link: [https://ucstaff-my.sharepoint.com/:f:/g/personal/ibrahim_radwan_canberra_edu_au/Er0nVreXmihEmtMz5qC5kVIB81-ugSusExPYdcyQTglfLg?e=bNO312]. 
Data, available with the following link: [https://www.transtats.bts.gov/Fields.asp?gnoyr_VQ=FGJ]. 


Required Libraries Uploaded

Code to be run in each cell separately

CSV files to be uploaded and extracted using relative path

File to be pushed to GITlab and commit for each version


Execution on AWS Sagemaker instructions:

- Create jupyter instance in aws sagemaker
- Select conda python3 kernel
- Upload combined_csv_v1 and combined_csv_v2 data files
- Upload oncloud files to be execute
- Execute files on kernel

